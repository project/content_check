<?php

namespace Drupal\content_check\Plugin\ContentCheckInput;

use Drupal\content_check\Plugin\ContentCheckInputBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Render an entity using the full view and return HTML.
 *
 * @ContentCheckInput(
 *   id = "rendered_entity_full_view",
 * )
 */
class RenderedEntityFullView extends ContentCheckInputBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getData($item) {
    $view_builder = $this->entityTypeManager->getViewBuilder($item->getEntity()->getEntityTypeId());
    $build = $view_builder->view($item->getEntity(), 'full', $item->getEntity()->language()->getId());
    return $this->renderer->render($build);
  }

}
